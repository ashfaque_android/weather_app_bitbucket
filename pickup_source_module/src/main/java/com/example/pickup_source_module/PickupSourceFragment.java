package com.example.pickup_source_module;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

public class PickupSourceFragment extends Fragment {

    private PickupSourceClickListener mListener;
    private RadioGroup mRadioGroup;
    private String mSelectedSource = "api";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pickup_source, container, false);
        mRadioGroup = view.findViewById(R.id.radio_group);
        // Method call to handle click events
        handleClickListener(view);
        return view;
    }

    /**
     * Handle click mListener
     */
    private void handleClickListener(View view) {
        // Go button click
        view.findViewById(R.id.btn_go).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onGoBtnClick(mSelectedSource);
            }
        });

        // Radio button change mListener
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == R.id.radio_mock){
                    mSelectedSource = "mock";
                } else {
                    mSelectedSource = "api";
                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof PickupSourceClickListener){
            mListener = (PickupSourceClickListener)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Interface definition for a callback to be invoked when view is clicked
     */
    public interface PickupSourceClickListener {
        void onGoBtnClick(String source);
    }
}
