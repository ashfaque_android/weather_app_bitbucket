package com.example.weather_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Datum_ {
    @SerializedName("time")
    @Expose
    public float time;
    @SerializedName("summary")
    @Expose
    public String summary;
    @SerializedName("icon")
    @Expose
    public String icon;
    @SerializedName("precipIntensity")
    @Expose
    public float precipIntensity;
    @SerializedName("precipProbability")
    @Expose
    public float precipProbability;
    @SerializedName("precipType")
    @Expose
    public String precipType;
    @SerializedName("temperature")
    @Expose
    public float temperature;
    @SerializedName("apparentTemperature")
    @Expose
    public float apparentTemperature;
    @SerializedName("dewPoint")
    @Expose
    public float dewPoint;
    @SerializedName("humidity")
    @Expose
    public float humidity;
    @SerializedName("pressure")
    @Expose
    public float pressure;
    @SerializedName("windSpeed")
    @Expose
    public float windSpeed;
    @SerializedName("windGust")
    @Expose
    public float windGust;
    @SerializedName("windBearing")
    @Expose
    public float windBearing;
    @SerializedName("cloudCover")
    @Expose
    public float cloudCover;
    @SerializedName("uvIndex")
    @Expose
    public float uvIndex;
    @SerializedName("visibility")
    @Expose
    public float visibility;
    @SerializedName("ozone")
    @Expose
    public float ozone;
    @SerializedName("precipAccumulation")
    @Expose
    public float precipAccumulation;
}
