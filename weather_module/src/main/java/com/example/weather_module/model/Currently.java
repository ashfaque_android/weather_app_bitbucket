package com.example.weather_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Currently {

    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("summary")
    @Expose
    public String summary;
    @SerializedName("icon")
    @Expose
    public String icon;
    @SerializedName("nearestStormDistance")
    @Expose
    public float nearestStormDistance;
    @SerializedName("nearestStormBearing")
    @Expose
    public float nearestStormBearing;
    @SerializedName("precipIntensity")
    @Expose
    public float precipIntensity;
    @SerializedName("precipProbability")
    @Expose
    public float precipProbability;
    @SerializedName("temperature")
    @Expose
    public String temperature;
    @SerializedName("apparentTemperature")
    @Expose
    public float apparentTemperature;
    @SerializedName("dewPoint")
    @Expose
    public float dewPoint;
    @SerializedName("humidity")
    @Expose
    public String humidity;
    @SerializedName("pressure")
    @Expose
    public float pressure;
    @SerializedName("windSpeed")
    @Expose
    public float windSpeed;
    @SerializedName("windGust")
    @Expose
    public float windGust;
    @SerializedName("windBearing")
    @Expose
    public float windBearing;
    @SerializedName("cloudCover")
    @Expose
    public float cloudCover;
    @SerializedName("uvIndex")
    @Expose
    public String uvIndex;
    @SerializedName("visibility")
    @Expose
    public String visibility;
    @SerializedName("ozone")
    @Expose
    public float ozone;
}
