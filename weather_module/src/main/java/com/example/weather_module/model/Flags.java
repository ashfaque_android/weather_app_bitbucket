package com.example.weather_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

class Flags {

    @SerializedName("sources")
    @Expose
    public List<String> sources = null;
    @SerializedName("nearest-station")
    @Expose
    public float nearestStation;
    @SerializedName("units")
    @Expose
    public String units;
}
