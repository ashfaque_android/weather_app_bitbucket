package com.example.weather_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Datum {
    @SerializedName("time")
    @Expose
    public float time;
    @SerializedName("precipIntensity")
    @Expose
    public float precipIntensity;
    @SerializedName("precipProbability")
    @Expose
    public float precipProbability;
}
