package com.example.weather_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Datum__ {
    @SerializedName("time")
    @Expose
    public float time;
    @SerializedName("summary")
    @Expose
    public String summary;
    @SerializedName("icon")
    @Expose
    public String icon;
    @SerializedName("sunriseTime")
    @Expose
    public float sunriseTime;
    @SerializedName("sunsetTime")
    @Expose
    public float sunsetTime;
    @SerializedName("moonPhase")
    @Expose
    public float moonPhase;
    @SerializedName("precipIntensity")
    @Expose
    public float precipIntensity;
    @SerializedName("precipIntensityMax")
    @Expose
    public float precipIntensityMax;
    @SerializedName("precipIntensityMaxTime")
    @Expose
    public float precipIntensityMaxTime;
    @SerializedName("precipProbability")
    @Expose
    public float precipProbability;
    @SerializedName("precipType")
    @Expose
    public String precipType;
    @SerializedName("temperatureHigh")
    @Expose
    public float temperatureHigh;
    @SerializedName("temperatureHighTime")
    @Expose
    public float temperatureHighTime;
    @SerializedName("temperatureLow")
    @Expose
    public float temperatureLow;
    @SerializedName("temperatureLowTime")
    @Expose
    public float temperatureLowTime;
    @SerializedName("apparentTemperatureHigh")
    @Expose
    public float apparentTemperatureHigh;
    @SerializedName("apparentTemperatureHighTime")
    @Expose
    public float apparentTemperatureHighTime;
    @SerializedName("apparentTemperatureLow")
    @Expose
    public float apparentTemperatureLow;
    @SerializedName("apparentTemperatureLowTime")
    @Expose
    public float apparentTemperatureLowTime;
    @SerializedName("dewPoint")
    @Expose
    public float dewPoint;
    @SerializedName("humidity")
    @Expose
    public float humidity;
    @SerializedName("pressure")
    @Expose
    public float pressure;
    @SerializedName("windSpeed")
    @Expose
    public float windSpeed;
    @SerializedName("windGust")
    @Expose
    public float windGust;
    @SerializedName("windGustTime")
    @Expose
    public float windGustTime;
    @SerializedName("windBearing")
    @Expose
    public float windBearing;
    @SerializedName("cloudCover")
    @Expose
    public float cloudCover;
    @SerializedName("uvIndex")
    @Expose
    public float uvIndex;
    @SerializedName("uvIndexTime")
    @Expose
    public float uvIndexTime;
    @SerializedName("visibility")
    @Expose
    public float visibility;
    @SerializedName("ozone")
    @Expose
    public float ozone;
    @SerializedName("temperatureMin")
    @Expose
    public float temperatureMin;
    @SerializedName("temperatureMinTime")
    @Expose
    public float temperatureMinTime;
    @SerializedName("temperatureMax")
    @Expose
    public float temperatureMax;
    @SerializedName("temperatureMaxTime")
    @Expose
    public float temperatureMaxTime;
    @SerializedName("apparentTemperatureMin")
    @Expose
    public float apparentTemperatureMin;
    @SerializedName("apparentTemperatureMinTime")
    @Expose
    public float apparentTemperatureMinTime;
    @SerializedName("apparentTemperatureMax")
    @Expose
    public float apparentTemperatureMax;
    @SerializedName("apparentTemperatureMaxTime")
    @Expose
    public float apparentTemperatureMaxTime;
    @SerializedName("precipAccumulation")
    @Expose
    public float precipAccumulation;
}
