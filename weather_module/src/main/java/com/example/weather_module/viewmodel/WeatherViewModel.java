package com.example.weather_module.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.weather_module.model.WeatherInfoModel;
import com.example.weather_module.source.remote.ApiClient;
import com.example.weather_module.source.remote.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.functions.Func2;

public class WeatherViewModel extends ViewModel {

    // This is the data that we will fetch asynchronously
    private MutableLiveData<WeatherInfoModel> weatherList;
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    // Call this method to get the data
    public LiveData<WeatherInfoModel> getWeatherInfo() {
        if (weatherList == null) {
            weatherList = new MutableLiveData<>();
            //we will load it asynchronously from server in this method
            loadWeatherInfo();
        }
        return weatherList;
    }

    public LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    /**
     * Method to load the weather info
     */
    private void loadWeatherInfo() {
        isLoading.setValue(true);
        /*ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<WeatherInfoModel> call = apiService.getWeatherInforLA();

        call.enqueue(new Callback<WeatherInfoModel>() {
            @Override
            public void onResponse(Call<WeatherInfoModel> call, Response<WeatherInfoModel> response) {
                //finally we are setting the list to our MutableLiveData
                weatherList.setValue(response.body());
                isLoading.setValue(false);
            }

            @Override
            public void onFailure(Call<WeatherInfoModel> call, Throwable t) {
                Log.e("=====>", t.toString());
                isLoading.setValue(false);
            }
        });*/

        Observable<WeatherInfoModel> weatherObservableLA = ApiClient.getClient()
                .create(ApiInterface.class)
                .getWeatherInforLA()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        Observable<WeatherInfoModel> weatherObservableNY = ApiClient.getClient()
                .create(ApiInterface.class)
                .getWeatherInfoNY()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());


        Observable<List<WeatherInfoModel>> combined = Observable.zip(weatherObservableLA, weatherObservableNY,
                new BiFunction<WeatherInfoModel, WeatherInfoModel, List<WeatherInfoModel>>() {
                    @Override
                    public List<WeatherInfoModel> apply(WeatherInfoModel weatherInfoModel, WeatherInfoModel weatherInfoModel2) throws Exception {
                        List<WeatherInfoModel> combinedRes = new ArrayList<>();
                        combinedRes.add(weatherInfoModel);
                        combinedRes.add(weatherInfoModel2);
                        return combinedRes;
                    }
                });
                /*// Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getObserver());*/
    }

    private Observer<List<WeatherInfoModel>> getObserver() {
        return new Observer<List<WeatherInfoModel>>() {

            @Override
            public void onSubscribe(Disposable d) {
                Log.d("=====>", " onSubscribe : " + d.isDisposed());
            }

            @Override
            public void onNext(List<WeatherInfoModel> userList) {
//                textView.append(" onNext");
//                textView.append(AppConstant.LINE_SEPARATOR);
//                for (User user : userList) {
//                    textView.append(" firstname : " + user.firstname);
//                    textView.append(AppConstant.LINE_SEPARATOR);
//                }
                Log.d("=====>", " onNext : " + userList.size());
            }

            @Override
            public void onError(Throwable e) {
//                textView.append(" onError : " + e.getMessage());
//                textView.append(AppConstant.LINE_SEPARATOR);
                Log.d("=====>", " onError : " + e.getMessage());
            }

            @Override
            public void onComplete() {
//                textView.append(" onComplete");
//                textView.append(AppConstant.LINE_SEPARATOR);
                Log.d("====>", " onComplete");
            }
        };
    }
}
