package com.example.weather_module.ui;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.weather_module.R;
import com.example.weather_module.ui.adapter.RecyclerViewAdapter;
import com.example.weather_module.model.WeatherInfoModel;
import com.example.weather_module.viewmodel.WeatherViewModel;

import java.util.ArrayList;
import java.util.List;

public class WeatherListFragment extends Fragment implements RecyclerViewAdapter.RecyclerViewItemClickListener {

    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mAdapter;
    private ProgressDialog mProgress;
    private WeatherViewModel mViewModel;
    private WeatherListClickListener mListener;
    private List<WeatherInfoModel> mWeatherInfoList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_list, container, false);

        mViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);

        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Loading...");
        mProgress.setCanceledOnTouchOutside(false);
        mRecyclerView = view.findViewById(R.id.recycler_view);

        // Use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new RecyclerViewAdapter(null, getActivity(), this);
        mRecyclerView.setAdapter(mAdapter);

//        weatherInfoServiceCall();
        mViewModel.getWeatherInfo().observe(this, new Observer<WeatherInfoModel>() {
            @Override
            public void onChanged(@Nullable WeatherInfoModel weatherDataModels) {
                mWeatherInfoList.add(weatherDataModels);
                mAdapter.notifyAdapter(mWeatherInfoList);
            }
        });

        mViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean aBoolean) {
                if (aBoolean) {
                    mProgress.show();
                } else {
                    mProgress.dismiss();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof WeatherListClickListener) {
            mListener = (WeatherListClickListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view, int position) {
        mListener.onWeatherListItemClick(view, position, mWeatherInfoList.get(position));
    }

    /**
     * Interface
     */
    public interface WeatherListClickListener {
        void onWeatherListItemClick(View view, int position, WeatherInfoModel model);
    }
}
