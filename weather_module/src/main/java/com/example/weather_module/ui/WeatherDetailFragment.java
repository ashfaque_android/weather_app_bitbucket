package com.example.weather_module.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.weather_module.R;
import com.example.weather_module.model.WeatherInfoModel;

public class WeatherDetailFragment extends Fragment {

    private WeatherInfoModel mWeatherInfoModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_detail, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mWeatherInfoModel = (WeatherInfoModel) getArguments().getSerializable("weather_info");

            ((TextView)view.findViewById(R.id.city)).setText("Los Angeles");
            ((TextView)view.findViewById(R.id.time)).setText(mWeatherInfoModel.currently.time);
            ((TextView)view.findViewById(R.id.summary)).setText(mWeatherInfoModel.currently.summary);
            ((TextView)view.findViewById(R.id.temprature)).setText(mWeatherInfoModel.currently.temperature);
            ((TextView)view.findViewById(R.id.humidity)).setText(mWeatherInfoModel.currently.humidity);
            ((TextView)view.findViewById(R.id.uvindex)).setText(mWeatherInfoModel.currently.uvIndex);
            ((TextView)view.findViewById(R.id.visibility)).setText(mWeatherInfoModel.currently.visibility);
        }

    }
}
