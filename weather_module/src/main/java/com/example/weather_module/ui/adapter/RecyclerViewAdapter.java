package com.example.weather_module.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.weather_module.R;
import com.example.weather_module.model.WeatherInfoModel;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private List<WeatherInfoModel> mWeatherData;
    private Context mContext;
    private RecyclerViewItemClickListener mListener;

    /**
     * Adapter
     */
    public RecyclerViewAdapter(List<WeatherInfoModel> weatherData, Context context, RecyclerViewItemClickListener listener) {
        mContext = context;
        mWeatherData = weatherData;
        mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new MyViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int i) {
        viewHolder.city.setText("Los Angeles");
        viewHolder.time.setText(mWeatherData.get(i).currently.time);
        ((MyViewHolder)viewHolder).summary.setText(mWeatherData.get(i).currently.summary);
        ((MyViewHolder)viewHolder).temprature.setText(mWeatherData.get(i).currently.temperature);
    }

    @Override
    public int getItemCount() {
        if (mWeatherData != null)
            return mWeatherData.size();
        return 0;
    }

    /**
     * View Holder class
     */
     public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView city;
        private TextView time;
        private TextView summary;
        private TextView temprature;
        private RecyclerViewItemClickListener mListener;

        public MyViewHolder(View view, RecyclerViewItemClickListener listener) {
            super(view);
            city = view.findViewById(R.id.city);
            time = view.findViewById(R.id.time);
            summary = view.findViewById(R.id.summary);
            temprature = view.findViewById(R.id.temprature);
            mListener = listener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v, getAdapterPosition());
        }
    }

    /**
     * Method to notify adapter
     */
    public void notifyAdapter(List<WeatherInfoModel> weatherData){
        mWeatherData = weatherData;
        notifyDataSetChanged();
    }

    /**
     * Interface
     */
    public interface RecyclerViewItemClickListener {
        void onClick(View view, int position);
    }
}
