package com.example.weather_module.source.remote;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.weather_module.utils.ApiConstant.BASE_URL;

public class ApiClient {

    private static Retrofit retrofit = null;

    // Private constructor
    private ApiClient(){

    }

    public static Retrofit getClient() {
        if(retrofit == null) {
            synchronized (ApiClient.class){
                if(retrofit == null) {
                    retrofit = new Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .build();
                }
            }
        }
        return retrofit;
    }

}
