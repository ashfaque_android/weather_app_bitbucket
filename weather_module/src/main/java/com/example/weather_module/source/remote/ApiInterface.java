package com.example.weather_module.source.remote;

import com.example.weather_module.model.WeatherInfoModel;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("37.3855,-122.088")
    Observable<WeatherInfoModel> getWeatherInforLA();

    @GET("40.7128,-74.0060")
    Observable<WeatherInfoModel> getWeatherInfoNY();
}
