package com.example.weatherapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.environment_module.EnvironmentFragment;
import com.example.pickup_source_module.PickupSourceFragment;
import com.example.weather_module.model.WeatherInfoModel;
import com.example.weather_module.ui.WeatherDetailFragment;
import com.example.weather_module.ui.WeatherListFragment;

public class MainActivity extends AppCompatActivity implements EnvironmentFragment.EnvironmentClickListener,
        PickupSourceFragment.PickupSourceClickListener, WeatherListFragment.WeatherListClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.container, new EnvironmentFragment());
            ft.addToBackStack("EnvironmentFragment");
            ft.commit();
        }
    }

    @Override
    public void onDevBtnClick() {
        replaceFragment(new PickupSourceFragment(), "PickupSourceFragment");
    }

    @Override
    public void onUatBtnClick() {
        replaceFragment(new PickupSourceFragment(), "PickupSourceFragment");
    }

    @Override
    public void onSubmitBtnClick() {
        replaceFragment(new PickupSourceFragment(), "PickupSourceFragment");
    }

    /**
     * Method to navigate on Pickup Source fragment
     */
    private void replaceFragment(Fragment fragment, String name) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(name);
        ft.commit();
    }

    @Override
    public void onGoBtnClick(String source) {
        replaceFragment(new WeatherListFragment(), "WeatherListFragment");
    }

    @Override
    public void onWeatherListItemClick(View view, int position, WeatherInfoModel model) {
        Fragment fragment = new WeatherDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("weather_info", model);
        fragment.setArguments(bundle);
        replaceFragment(fragment, "WeatherDetailFragment");
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            MainActivity.this.finish();
        }
    }
}
