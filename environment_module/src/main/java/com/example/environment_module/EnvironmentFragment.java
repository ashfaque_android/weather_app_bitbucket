package com.example.environment_module;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

public class EnvironmentFragment extends Fragment {

    private Button btnSubmit;
    private EnvironmentClickListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_environment, container, false);

        btnSubmit = view.findViewById(R.id.btn_submit);
        btnSubmit.setVisibility(View.GONE);
        // Method call to handle click events
        handleClickListener(view);
        // Method call to handle textwatcher
        handleTextChangeListener(view);
        return view;
    }

    /**
     * Handle click listener
     */
    private void handleClickListener(View view) {
        // DEV button click
        view.findViewById(R.id.btn_dev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDevBtnClick();
            }
        });
        // UAT button click
        view.findViewById(R.id.btn_UAT).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onUatBtnClick();
            }
        });
        // Submit button click
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSubmitBtnClick();
            }
        });
    }

    /**
     * Handle text changed listener
     */
    private void handleTextChangeListener(View view){
        // Edit text change listener
        ((EditText)view.findViewById(R.id.editText)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count > 0){
                    btnSubmit.setVisibility(View.VISIBLE);
                } else {
                    btnSubmit.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof EnvironmentClickListener){
            listener = (EnvironmentClickListener)context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    /**
     * Interface definition for a callback to be invoked when view is clicked
     */
    public interface EnvironmentClickListener {
        void onDevBtnClick();
        void onUatBtnClick();
        void onSubmitBtnClick();
    }
}
